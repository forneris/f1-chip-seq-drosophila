---
title: "Figure_S1"
output:
   BiocStyle::html_document:
      toc: true
      df_print: paged
      self_contained: true
      code_download: true
      highlight: tango
#bibliography: knn_ml_intro.bib
editor_options: 
  chunk_output_type: inline
---
---
title: "WASP benchmarks"
output:
   BiocStyle::html_document:
      toc: true
      df_print: paged
      self_contained: true
      code_download: true
      highlight: tango
#bibliography: knn_ml_intro.bib
editor_options: 
  chunk_output_type: inline
---

```{r style, echo=FALSE, results="asis"}
library("knitr")
options(digits = 2, width = 80)
options(bitmapType = 'cairo')
golden_ratio <- (1 + sqrt(5)) / 2
opts_chunk$set(echo = TRUE, tidy = FALSE, include = TRUE,
               dev=c('png', 'pdf'), fig.height = 6, fig.width = 6, comment = '  ', dpi = 300, warning = FALSE, message=FALSE)
```

```{r}
source("../utils/utils.R")
config = load_config()

# load CHT results
cht_full = lapply(ab_tp_list, function(ab_tp) load_cht_results(ab_tp, remove_chr = F)) %>% bind_rows()
cht = cht_full %>% filter(!TEST.SNP.CHROM %in% c("chrX", "chrY", "chrM"))
cht_sign = cht %>% filter(signif_strongAI) 

# genes and promoters
genes = load_genes()
promoters = resize(genes, width = 1000, fix = "start")

# combined motif set (all TFs, peaks + alleles)
fimo = get_full_motif_sets(cht, ab_tp_list)
# only alleles
fimo_alleles  = lapply(ab_tp_list, function(ab_tp) parse_motifs_in_two_alleles(ab_tp, cht)) %>% bind_rows() 
```

# Gained reads with new WASP filtering including INDELs

```{r compared_reads_with_Without_INDELs, comment=NA, echo=FALSE, message=FALSE}

reads_with_VS_withoutINDELs = read.table("/g/furlong/project/68_F1_cisreg_ichip/analysis/mappability_filter/mapping_pipeline_WASP_logs/counts/duplicated_reads_filtered_with_VS_without_INDELs.txt", header=TRUE)
reads_with_VS_withoutINDELs$gain = reads_with_VS_withoutINDELs$with_INDELs / reads_with_VS_withoutINDELs$without_INDELs
reads_with_VS_withoutINDELs$antibody = factor(reads_with_VS_withoutINDELs$antibody, levels = c("bin", "ctcf", "mef2", "twi", "input"))

p = ggplot(reads_with_VS_withoutINDELs, aes(x=antibody, y=gain, colour=antibody)) +
    geom_jitter(width=0.35) +
    geom_boxplot(fill=NA, colour="grey10", alpha=0.4, width=0.2, outlier.color = NA) + 
    geom_hline(yintercept = 1, linetype="dashed", colour="grey40", size=1.5) +
    scale_y_continuous(labels = scales::percent, limits=c(1,2)) +
    xlab("Samples by antibody") +
    ylab("Percentage of read ganed with new WASP pipeline") + 
    theme_bw() + 
    theme(legend.position="none") +
    theme(panel.grid = element_line(colour = "grey80", linewidth = 1), axis.text = element_text(size = 9)) +
    theme(axis.title = element_text(size = 9), plot.title = element_text(size=9)) +
    theme(panel.grid.minor = element_line(linewidth = 0.25), panel.grid.major = element_line(linewidth = 0.5)) 

p
ggsave(file.path(outdir_fig_suppl, "FigS1_WASP_filtering_including_INDELs.pdf"), p, width = 6, height = 6)
```

# Number of reads filtered along steps of pipeline

```{r filtered_reads_WASP, comment=NA, echo=FALSE, message=FALSE, fig.height = 6, fig.width = 20}

WASP_filters = read.table("/g/furlong/project/68_F1_cisreg_ichip/analysis/mappability_filter/mapping_pipeline_WASP_logs/counts/WASP_filtered_reads_whole_pipeline_withINDELs_samples.txt", header=TRUE)
WASP_filters$total_reads_ratio = WASP_filters$n_reads / WASP_filters$n_reads
WASP_filters$mapped_reads_ratio = (WASP_filters$mapped_reads / WASP_filters$n_reads) / 2
WASP_filters$passed_MAPQ_filter_ratio = (WASP_filters$passed_MAPQ_filter / WASP_filters$n_reads) / 2
WASP_filters$passed_pre.WASP_QC_ratio = (WASP_filters$passed_pre.WASP_QC / WASP_filters$n_reads) / 2
WASP_filters$mappability_filter_ratio = (WASP_filters$mappability_filter / WASP_filters$n_reads) / 2
WASP_filters$duplicate_reads_filtered_ratio = (WASP_filters$duplicate_reads_filtered / WASP_filters$n_reads) / 2

WASP_filter_long = WASP_filters %>%
 pivot_longer(!c("sample_name", "n_reads", "mapped_reads", "passed_MAPQ_filter", "passed_pre.WASP_QC", "mappability_filter", "duplicate_reads_filtered", "condition", "antibody", "cross", "rep" ), names_to = "filter", values_to = "ratio")
WASP_filter_long$filter = gsub("total_reads_ratio", "total reads", 
                               gsub("mapped_reads_ratio", "mapped reads", 
                               gsub("passed_MAPQ_filter_ratio", "passed MAPQ filters", 
                               gsub("passed_pre.WASP_QC_ratio", "passed pre-WASP QC",
                               gsub("mappability_filter_ratio", "mappability filter",
                               gsub("duplicate_reads_filtered_ratio", "duplicates removed",
                                WASP_filter_long$filter))))))
WASP_filter_long$filter = factor(WASP_filter_long$filter, rev(c("total reads", "mapped reads", "passed MAPQ filters", "passed pre-WASP QC", "mappability filter", "duplicates removed")))
WASP_filter_long$antibody = factor(WASP_filter_long$antibody, levels = c("bin", "ctcf", "mef2", "twi", "input"))


WASP_filter_long_noInput = subset(WASP_filter_long, condition != "input.24" & condition != "input.68" & condition != "input.1012")

p = ggplot(WASP_filter_long, aes(x=filter, y=ratio, colour=antibody)) +
    geom_jitter(width=0.4, size=1) +
    geom_boxplot(fill=NA, colour="grey10", alpha=0.4, width=0.2, outlier.color = NA) +
    facet_wrap(facets = "antibody", ncol = 5) +
    xlab("QC filtering step") +
    ylab("Percentage of reads kept along filtering steps") + 
    scale_y_continuous(labels = scales::percent, limits=c(0,1.001)) +
    coord_flip() +
    theme_bw() + 
    theme(legend.position="none") +
    theme(panel.grid = element_line(colour = "grey80", linewidth = 1), axis.text = element_text(size = 9)) +
    theme(axis.title = element_text(size = 9), plot.title = element_text(size=9)) +
    theme(strip.text = element_text(size=9)) +
    theme(panel.grid.minor = element_line(linewidth = 0.25), panel.grid.major = element_line(linewidth = 0.5)) + 
    theme(panel.spacing = unit(2, "lines"))

p
ggsave(file.path(outdir_fig_suppl, "FigS1_Number_of_reads_filtered.pdf"), p, width = 6, height = 6)
```


# Percent of reads filtered along WASP pipeline

```{r filtered_reads_WASP_pipeline, comment=NA, echo=FALSE, message=FALSE, fig.height = 6, fig.width = 20}
WASP_filtered_reads = read.table("/g/furlong/project/68_F1_cisreg_ichip/analysis/mappability_filter/mapping_pipeline_WASP_logs/counts/filtering_within_WASP_counts.txt", header=TRUE)

WASP_filtered_reads$total_reads_ratio = WASP_filtered_reads$Tot_reads / WASP_filtered_reads$Tot_reads 
WASP_filtered_reads$INDEL_reads_ratio = (WASP_filtered_reads$INDEL_reads / WASP_filtered_reads$Tot_reads) 
WASP_filtered_reads$Mappability_filtered_ratio = (WASP_filtered_reads$Mappability_filtered / WASP_filtered_reads$Tot_reads) 
WASP_filtered_reads$Allelic_combinations_ratio = (WASP_filtered_reads$Allelic_combinations / WASP_filtered_reads$Tot_reads) 
WASP_filtered_reads$Improper_mapping_ratio = (WASP_filtered_reads$Improper_mapping / WASP_filtered_reads$Tot_reads) 


WASP_filtered_reads_long = WASP_filtered_reads %>%
 pivot_longer(!c("sample_name", "Tot_reads", "INDEL_reads", "Mappability_filtered", "Allelic_combinations", "Improper_mapping", "condition", "antibody", "cross", "rep" ), names_to = "filter", values_to = "ratio")
WASP_filtered_reads_long$filter = gsub("total_reads_ratio", "total reads", 
                               gsub("INDEL_reads_ratio", "INDELs", 
                               gsub("Mappability_filtered_ratio", "Mappability issues",
                               gsub("Allelic_combinations_ratio", "Allele combinations",
                               gsub("Improper_mapping_ratio", "Improper mapping",
                                WASP_filtered_reads_long$filter)))))
WASP_filtered_reads_long$filter = factor(WASP_filtered_reads_long$filter, rev(c("total reads", "INDELs", "Mappability issues", "Allele combinations", "Improper mapping")))
WASP_filtered_reads_long$antibody = factor(WASP_filtered_reads_long$antibody, levels = c("bin", "ctcf", "mef2", "twi", "input"))

WASP_filtered_reads_long = subset(WASP_filtered_reads_long, filter != "total reads")

p = ggplot(WASP_filtered_reads_long, aes(x=filter, y=ratio, colour=antibody)) +
    geom_jitter(width=0.4, size=1) +
    geom_boxplot(fill=NA, colour="grey10", alpha=0.4, width=0.2, outlier.color = NA) +
    facet_wrap(facets = "antibody", ncol = 5) +
    xlab("WASP filtering steps") +
    ylab("Percentage of reads removed by WASP filtering") + 
    scale_y_continuous(labels = scales::percent, limits=c(0,0.30)) +
    coord_flip() +
    theme_bw() + 
    theme(legend.position="none") +
    theme(panel.grid = element_line(colour = "grey80", linewidth = 1), axis.text = element_text(size = 9)) +
    theme(axis.title = element_text(size = 9), plot.title = element_text(size=9)) +
    theme(strip.text = element_text(size=9)) +
    theme(panel.grid.minor = element_line(linewidth = 0.25), panel.grid.major = element_line(linewidth = 0.5)) + 
    theme(panel.spacing = unit(2, "lines"))

p
ggsave(file.path(outdir_fig_suppl, "FigS1_Number_of_reads_filtered_WASP_pipeline.pdf"), p, width = 6, height = 6)
```


* Maximum percentage of passed MAPQ filters reads that overlap an INDEL: `r max(WASP_filtered_reads$INDEL_reads_ratio)*100`% minimum: `r min(WASP_filtered_reads$INDEL_reads_ratio)*100`%


* Maximum percentage of mappability filtered reads by the new WASP pipeline: `r max(WASP_filtered_reads$Mappability_filtered_ratio)*100`% minimum: `r min(WASP_filtered_reads$Mappability_filtered_ratio)*100`%




# Distribution of INDEL size

```{r }
vcf = read.table(paste0(config$data$VCF$vcf_dir, "/", config$data$VCF$stringent))[, c("V1", "V2", "V3", "V4", "V5")]
colnames(vcf) = c("chr", "start", "end", "REF", "ALT")
vcf$REF_len = apply(vcf,2,nchar)[,c("REF")]
vcf$ALT_len = apply(vcf,2,nchar)[,c("ALT")]

INDELs = subset(vcf, REF_len != ALT_len)
INDELs$INDEL_len = pmax(INDELs$REF_len, INDELs$ALT_len) - 1

p = ggplot(INDELs, aes(x=INDEL_len)) +
    geom_histogram(bins = 26, fill="grey60") + 
    xlab("Length of INDELs (bp)") +
    ylab("Number of INDELs") +
    xlim(0,25) +
    geom_vline(xintercept=1.5, linewidth=0.5, linetype="dashed", colour="grey30") +
    geom_vline(xintercept=5.5, linewidth=0.5, linetype="dashed", colour="grey30") +
    geom_vline(xintercept=10.5, linewidth=0.5, linetype="dashed", colour="grey30") +
    annotate(x = 18, y=80000, geom = "text", label="number of INDEL by size", size=3) +
    annotate(x = 18, y=70000, geom = "text", label=paste0("1bp = ", nrow(subset(INDELs, INDEL_len==1)), " (", round(nrow(subset(INDELs, INDEL_len==1))/nrow(INDELs), 3)*100, "%)"), size=2.5) +
    annotate(x = 18, y=60000, geom = "text", label=paste0("2-5bp = ", nrow(subset(INDELs, INDEL_len>=2 & INDEL_len<=5)), " (", round(nrow(subset(INDELs, INDEL_len>=2 & INDEL_len<=5))/nrow(INDELs), 3)*100, "%)"), size=2.5) +
    annotate(x = 18, y=50000, geom = "text", label=paste0("6-10bp = ", nrow(subset(INDELs, INDEL_len>=6 & INDEL_len<=10)), " (", round(nrow(subset(INDELs, INDEL_len>=6 & INDEL_len<=10))/nrow(INDELs), 3)*100, "%)"), size=2.5) +
    annotate(x = 18, y=40000, geom = "text", label=paste0(">10bp = ", nrow(subset(INDELs, INDEL_len>10)), " (", round(nrow(subset(INDELs, INDEL_len>10))/nrow(INDELs), 3)*100, "%)"), size=2.5) +
    theme_bw() + 
    theme(legend.position="none") +
    theme(panel.grid = element_line(colour = "grey80", linewidth = 1), axis.text = element_text(size = 9)) +
    theme(axis.title = element_text(size = 9), plot.title = element_text(size=9)) +
    theme(strip.text = element_text(size=9)) +
    theme(panel.grid.minor = element_line(linewidth = 0.25), panel.grid.major = element_line(linewidth = 0.5)) 

p
ggsave(file.path(outdir_fig_suppl, "FigS1C_INDELs_distribution_size.pdf"), p, width = 4, height = 4)
```



# Distribution of INDEL size after CHT test (WASP filtered)

```{r }
filtered_variants = unique(cht[, c("TEST.SNP.ID", "TEST.SNP.REF.ALLELE", "TEST.SNP.ALT.ALLELE")])
colnames(filtered_variants) = c("variant_ID", "REF", "ALT")
filtered_variants$REF_len = apply(filtered_variants,2,nchar)[,c("REF")]
filtered_variants$ALT_len = apply(filtered_variants,2,nchar)[,c("ALT")]

filtered_INDELs = subset(filtered_variants, REF_len != ALT_len)
filtered_INDELs$INDEL_len = pmax(filtered_INDELs$REF_len, filtered_INDELs$ALT_len) - 1

p = ggplot(filtered_INDELs, aes(x=INDEL_len)) +
    geom_histogram(bins = 26, fill="grey60") + 
    xlab("Length of INDELs (bp)") +
    ylab("Number of INDELs") +
    xlim(0,25) +
    geom_vline(xintercept=1.5, linewidth=0.5, linetype="dashed", colour="grey30") +
    geom_vline(xintercept=5.5, linewidth=0.5, linetype="dashed", colour="grey30") +
    geom_vline(xintercept=10.5, linewidth=0.5, linetype="dashed", colour="grey30") +
    annotate(x = 18, y=20000, geom = "text", label="number of INDEL by size", size=3) +
    annotate(x = 18, y=18000, geom = "text", label=paste0("1bp = ", nrow(subset(filtered_INDELs, INDEL_len==1)), " (", round(nrow(subset(filtered_INDELs, INDEL_len==1))/nrow(filtered_INDELs), 3)*100, "%)"), size=2.5) +
    annotate(x = 18, y=16000, geom = "text", label=paste0("2-5bp = ", nrow(subset(filtered_INDELs, INDEL_len>=2 & INDEL_len<=5)), " (", round(nrow(subset(filtered_INDELs, INDEL_len>=2 & INDEL_len<=5))/nrow(filtered_INDELs), 3)*100, "%)"), size=2.5) +
    annotate(x = 18, y=14000, geom = "text", label=paste0("6-10bp = ", nrow(subset(filtered_INDELs, INDEL_len>=6 & INDEL_len<=10)), " (", round(nrow(subset(filtered_INDELs, INDEL_len>=6 & INDEL_len<=10))/nrow(filtered_INDELs), 3)*100, "%)"), size=2.5) +
    annotate(x = 18, y=12000, geom = "text", label=paste0(">10bp = ", nrow(subset(filtered_INDELs, INDEL_len>10)), " (", round(nrow(subset(filtered_INDELs, INDEL_len>10))/nrow(filtered_INDELs), 3)*100, "%)"), size=2.5) +
    theme_bw() + 
    theme(legend.position="none") +
    theme(panel.grid = element_line(colour = "grey80", linewidth = 1), axis.text = element_text(size = 9)) +
    theme(axis.title = element_text(size = 9), plot.title = element_text(size=9)) +
    theme(strip.text = element_text(size=9)) +
    theme(panel.grid.minor = element_line(linewidth = 0.25), panel.grid.major = element_line(linewidth = 0.5)) 

p
ggsave(file.path(outdir_fig_suppl, "FigS1D_INDELs_after_WASP_filter_distribution_size.pdf"), p, width = 4, height = 4)
```