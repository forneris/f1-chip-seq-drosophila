---
title: "Figure_1"
output:
   BiocStyle::html_document:
      toc: true
      df_print: paged
      self_contained: true
      code_download: true
      highlight: tango
#bibliography: knn_ml_intro.bib
editor_options: 
  chunk_output_type: inline
---

```{r style, echo=FALSE, results="asis"}
library("knitr")
options(digits = 2, width = 80)
options(bitmapType = 'cairo')
golden_ratio <- (1 + sqrt(5)) / 2
opts_chunk$set(echo = TRUE, tidy = FALSE, include = TRUE, cache = FALSE,
               dev=c('png', 'pdf'), comment = '  ', dpi = 300)

options(stringsAsFactors = FALSE)
knitr::opts_chunk$set(cache=FALSE)
options(digits = 5)         
```


# Setup and data

```{r}
source("../utils/utils.R")
config = load_config()

# load CHT results
cht_full = lapply(ab_tp_list, function(ab_tp) load_cht_results(ab_tp, remove_chr = F)) %>% bind_rows()
cht = cht_full %>% filter(!TEST.SNP.CHROM %in% c("chrX", "chrY", "chrM"))
cht_sign = cht %>% filter(signif_strongAI) 

# genes and promoters
genes = load_genes()
promoters = resize(genes, width = 1000, fix = "start")

# combined motif set (all TFs, peaks + alleles)
fimo = get_full_motif_sets(cht, ab_tp_list)
# only alleles
fimo_alleles  = lapply(ab_tp_list, function(ab_tp) parse_motifs_in_two_alleles(ab_tp, cht)) %>% bind_rows() 
```


# Figure 1 C motif logos


```{r}

outf_base = file.path(outdir_fig_main, "/motif_logos/")
f = config$data$motif_databases$denovo_motifs
pfms = read_meme(f)

for(pfm in pfms) {
  
  #motif_name = gsub("\\/", "_", pfm@name)
  motif_name = pfm@name
  print(motif_name)
  outf = file.path(outf_base, paste0(motif_name, ".pdf"))
  
  p = view_motifs(pfm, use.type = "ICM", colour.scheme = letter_colors, sort.positions = T) +
    theme_classic() +
    scale_x_continuous(breaks = 1:ncol(pfm), labels = 1:ncol(pfm), name = "Position") +
    ylab("Bits") +
    theme(axis.text.y = element_text(size=12), axis.text.x = element_text(size=12),
          axis.title.y = element_text(size=12), axis.title.x = element_text(size=12),
          legend.position="none",
          plot.title = element_text(color="black", face="bold", size=16, hjust=0.5))
  
  print(p)
  ggsave(outf, p,  width = 4, height = 2)
  
  
}

```


# Figure 1 C Motif central enrichment

```{r}
pp = lapply(ab_tp_list, function(ab_tp) {print(ab_tp); plot_motif_central_enrichment(ab_tp, pwm = motifs_denovo_list[ab_tp])})

p = do.call("grid.arrange", c(pp, ncol= 6))
ggsave(file.path(outdir_fig_main, "Fig1C_motif_central_enrichment.pdf"), p, width = 15, height = 2)

```


# Figure 1 D Upset plot

```{r}
# paths to consensus peak sets for all TFs
ff = lapply(ab_tp_list, function(ab_tp) get_path_consensus_peakset(ab_tp))

# read peak sets
peaks = lapply(ff, function(f) load_consensus_peak_set(f, filter_chr = F)) %>% GRangesList()
names(peaks) = ab_tp_labels

combined_peakset = construct_combined_consensus_set(peaks)

df = combined_peakset[ , 6:ncol(combined_peakset)]
names(df) = ab_tp_labels


#{pdf(file.path(outdir_fig_main, "Fig1D_UpsetPlot_full.pdf"), width = 7, height = 5)

p = upset(df,  order.by = "freq", nintersects = 15, keep.order = T, sets = rev(names(df)),
      sets.bar.color = rev(TFcols), text.scale = 1.5)

print(p)

pdf(file.path(outdir_fig_main, "Fig1D_UpsetPlot_full.pdf"), width = 7, height = 5)
p
dev.off()

```


# Figure 1 E Number of variants per peak

```{r}
# paths to consensus peak sets for all TFs
ff = lapply(ab_tp_list, function(ab_tp) get_path_consensus_peakset(ab_tp))
peaks = lapply(ff, function(f) load_consensus_peak_set(f, filter_chr = F) %>% as.data.frame()) %>% bind_rows() %>% select(peak_id, condition)

# annotate by number of variants per peak
peaks_with_variants = get_number_of_variants_per_peak(cht_full)

df = merge(peaks, peaks_with_variants, by = c("peak_id", "condition"), all.x = T)
df$n_var[is.na(df$n_var)] = 0

df %<>% mutate(bin_n_var = cut(n_var, breaks = c(-1, 0, 20, 50, 80, Inf), labels = c("0", "1-20", "21-50", "51-80", ">80")))

df_sum = df %>% group_by(condition) %>% 
        mutate(n_tot = n()) %>%
        group_by(condition, bin_n_var) %>%
        summarise(n_bin_var = n(), share_bin_var = n_bin_var / mean(n_tot)) %>%
        mutate(bin_n_var = factor(bin_n_var, levels = rev(levels(bin_n_var))))

df_sum$label = factor(ab_tp_labels[df_sum$condition], levels = rev(ab_tp_labels))

p = ggplot(df_sum, aes(x = label, y = share_bin_var, fill = bin_n_var)) +
  geom_bar(stat = "identity", width = 0.6) +
  scale_fill_manual(values = cbPalette, name = "# variants per peak") +
  xlab("") +
  ylab("Share of peaks") +
  theme_bw() +
  ylim(c(0, 1)) +
  coord_flip() +
  theme(axis.text.y = element_text(size=18, color = rev(TFcols)), axis.text.x = element_text(size=16), 
        axis.title.x = element_text(size=18), axis.title.y = element_text(size=16),
        legend.text=element_text(size=16), legend.title=element_text(size=16))


p
ggsave(file.path(outdir_fig_main, "Fig1E_num_vriants_per_peak.pdf"), p, width = 8, height = 3)

```