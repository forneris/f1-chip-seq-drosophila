#!/bin/bash
#SBATCH -A furlong                  # group to which you belong
#SBATCH -N 1                        # number of nodes
#SBATCH -n 16                        # number of cores
#SBATCH --mem 40G                   # memory pool for all cores
#SBATCH -t 2-12:00                   # runtime limit (D-HH:MM:SS)
#SBATCH -o cluster_log/diversity.%N.%j.out          # STDOUT
#SBATCH -e cluster_log/diversity.%N.%j.err          # STDERR
#SBATCH --mail-type=FAIL                # notifications for job done & fail
#SBATCH --mail-user=olga.sigalova@embl.de   # send-to address


module load Python/2.7.14-foss-2017b
#export PATH=/g/furlong/sigalova/software/DIVERSITY-1.0.0/DIVERSITY-1.0.0:$PATH

d_path=/g/furlong/sigalova/software/DIVERSITY-1.0.0/DIVERSITY-1.0.0
fi=/g/furlong/project/68_F1_cisreg_ichip/analysis/ChIPseq/Motif_analysis/with_indels/merged_input_idr0.01_ind3/mef2/68/consensus_peaks.fa
fo=/g/furlong/project/68_F1_cisreg_ichip/analysis/ChIPseq/Motif_analysis/with_indels/merged_input_idr0.01_ind3/mef2/68/cosensus_peaks_diversity


python $d_path/diversityMain.py -f $fi -o $fo -proc 16 -fast 2 -minWidth 8 -maxMode 8
