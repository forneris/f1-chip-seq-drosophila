#!/bin/bash

########################
# PATHS AND PARAMETERS #
########################

# All parameters and paths are defined here:
. "./params.sh"



########################
# RUN AUTOMATED SCRIPT #
########################
. "/g/furlong/project/68_F1_cisreg_ichip/src/shared/runSnakemakeWrapper.sh"
#. "/g/scb/zaugg/zaugg_shared/scripts/Christian/src/Snakemake/runSnakemakeWrapper.sh"
#. "/g/furlong/project/68_F1_cisreg_ichip/src/shared/runSnakemakeWrapper_new.sh"
