configfile: "../1_preprocess_fastq_files/config.yml"

import os
import sys
import yaml
import pandas as pd
from snakemake.utils import min_version
# Enforce a minimum Snakemake version
min_version("4.7")

onstart:
    print("##########################################\n")
    print("# STARTING PIPELINE\n")
    print("##########‰################################\n")
    print ("Running ChIP-seq pre-processing workflow for the following samples:\n " + ' \n '.join(map(str, sample_names)))

onsuccess:
    print("##########################################\n")
    print("# PIPELINE ENDED SUCCESSFULLY \n")
    print("##########################################\n")

ruleDisplayMessage = """\n\n####### START RULE EXECUTION ##########\n"""

################################################################################
######################## FUNCTIONS #############################################
################################################################################

def get_fastq_read_path(samples2paths, sample_name, read):
    """Get path to fastq by sample name.
       Input:
        samples2paths (dict): {sample_name: [path_to_read1, path_to_read2]}
        sample_name (str): must correspond to keys from the dictionary
        read (str): either read1 or read2
    """
    return samples2paths[sample_name][read]

################################################################################
######################## READ DATA #############################################
################################################################################

### FOLDERS ON TIER1 ###
PROJECT_DIR = config["global"]["projectdir"]
DATA_DIR  = PROJECT_DIR + "/" + config["project_structure"]["datadir"]
LOG_DIR = PROJECT_DIR + "/" + config["project_structure"]["logdir"] + "/preprocess_fastq"
COUNT_READS = PROJECT_DIR + "/" + "analysis/n_reads"
PROBOUND_DIR = PROJECT_DIR + "/" + "analysis/ProBound"

### FOLDERS ON SCRATCH ###
SCRATCH_DIR = config["global"]["scratchdir"]
ADAPTER_CLIP_DIR = SCRATCH_DIR + "/PREPROCESS_FASTQ/1.JE_CLIP"
SKEWER_DIR =  SCRATCH_DIR + "/PREPROCESS_FASTQ/2.SKEWER"
SEQTK_DIR = SCRATCH_DIR + "/PREPROCESS_FASTQ/3.SEQTK_TRIMFQ"


### SOFTWARE ###
JE = config["tools"]["by_path"]["je"]["v1"]
SKEWER = config["tools"]["by_path"]["skewer"]
SEQTK = config["tools"]["by_module"]["seqtk"]
SEQTK = config["tools"]["by_path"]["seqtk"]
PROBOUND = config["tools"]["by_path"]["ProBound_jar"]

### DATA ###

# samples info
samplesSummaryFile = config["data"]["samples"]["sample_table"]
samples_info = pd.read_csv(samplesSummaryFile, sep = ",")

# generate names of samples in format: antibody.mother.father.time.replicate
nrows = samples_info.shape[0]
sample_names = []
for i in range(0, nrows):
    genotype_rep = "_".join((map(str, samples_info.loc[i, [ "mother", "father", "replicate"]].tolist())))
    ab_tp = ".".join((map(str, samples_info.loc[i, [ "Antibody", "timePoint"]].tolist())))
    sample_name = ".".join([ab_tp, genotype_rep])
    sample_names.append(sample_name)

# paths to read1 and read2
read1_paths = samples_info["read1_path"].tolist()
read2_paths = samples_info["read2_path"].tolist()

# dictionary with sample names as keys and paths to read1 and read2 as values
reads2paths = [{'read1': read1, 'read2': read2} for read1, read2 in zip(read1_paths, read2_paths)]
samples2paths = dict(zip(sample_names, reads2paths))
print(reads2paths)

# ProBound TF to ID
PROBOUND_IDs = {"CTCF": 12715, "Mef2": 13466, "Bin": 15352, "Twi": 15424}
ALLELES = ["REF", "ALT"]
print(PROBOUND_IDs)

# Targets
SEQUENCES_VARIANTS_OUT = expand("{path}/{allele}_sequence_15bp_around_variants.fa", path=PROBOUND_DIR, allele=ALLELES)
PROBOUND_OUT = expand("{path}/ProBound_{TF}_{allele}_sequence_15bp_around_variants.txt", path=PROBOUND_DIR, TF=PROBOUND_IDs.keys(), allele=ALLELES)


rule all:
    input:
        SEQUENCES_VARIANTS_OUT, PROBOUND_OUT



rule sequence_around_variants:
    input:
        variants_scores = "/g/furlong/project/68_F1_cisreg_ichip/data/for_Frosina/quantified_variants_all_conditions.txt"
    output:
        expand("{path}/REF_sequence_15bp_around_variants.fa", path=PROBOUND_DIR)
    params:
        genome = config["data"]["genome"]["dm6"]["fasta"]
    envmodules:
        "BEDTools/2.30.0-GCC-12.2.0 "
    shell:
        """
        bedtools getfasta -name -fi {params.genome} -bed <(cat {input.variants_scores} \
        | tail -n +2 | sed 's/\\ /\\t/g; s/\\"//g' \
        | awk '{{print $1 "\\t" $2-31 "\\t" $2+30 "\\t" $1 "_" $2 "_" $3 "_" $6 "_" $7}}'  \
        | sort -k1,1 -k2,2g | uniq) > {output}
        """


rule sequence_around_variants_ALT:
    input:
        variants_scores = "/g/furlong/project/68_F1_cisreg_ichip/data/for_Frosina/quantified_variants_all_conditions.txt"
    output:
        expand("{path}/ALT_sequence_15bp_around_variants.fa", path=PROBOUND_DIR)
    params:
        genome = config["data"]["genome"]["dm6"]["fasta"]
    envmodules:
        "BEDTools/2.30.0-GCC-12.2.0 "
    shell:
        """
        paste <(bedtools getfasta -name -fi {params.genome} -bed <(cat {input.variants_scores} \
        | tail -n +2 | sed 's/\\ /\\t/g; s/\\"//g' | awk 'length($6) == 1 && length($7) == 1'  \
        | awk '{{print $1 "\\t" $2-31 "\\t" $2-1 "\\t" $1 "_" $2 "_" $3 "_" $6 "_" $7}}'  \
        | sort -k1,1 -k2,2g | uniq)) \
        <(cat {input.variants_scores} | tail -n +2 | sed 's/\\ /\\t/g; s/\\"//g' \
        | awk 'length($6) == 1 && length($7) == 1' | cut -f1-7 | sort -k1,1 -k2,2g \
        | uniq | awk '{{print "\\n" $7}}') \
        <(bedtools getfasta -name -fi {params.genome} -bed <(cat {input.variants_scores} \
        | tail -n +2 | sed 's/\\ /\\t/g; s/\\"//g' | awk 'length($6) == 1 && length($7) == 1' \
        | awk '{{print $1 "\\t" $2 "\\t" $2+30 "\\t" $1 "_" $2 "_" $3 "_" $6 "_" $7}}'  \
        | sort -k1,1 -k2,2g | uniq)) \
        | sed 's/>[A-Za-z0-9_:-]*//2; s/\\t//g' > {output}
        """


rule run_ProBound:
    input:
        sequences = expand("{path}/{{allele}}_sequence_15bp_around_variants.fa", path=PROBOUND_DIR)
    output:
        expand("{path}/ProBound_{{TF}}_{{allele}}_sequence_15bp_around_variants.txt", path=PROBOUND_DIR)
    params:
        ProBound_bin = PROBOUND,
        TF_ID = lambda wildcards: PROBOUND_IDs[wildcards.TF]
    envmodules:
        "GATK/4.2.3.0-GCCcore-11.2.0-Java-11"
    shell:
        """
        cat {input.sequences} | grep -v "^>" > {output}.temp.fa

        java -cp {params.ProBound_bin} proBoundTools/App \
        -c 'loadMotifCentralModel({params.TF_ID}).addNScoring().inputTXT({output}.temp.fa).bindingModeScores(/dev/stdout)' \
        > {output}.temp.ProBound.txt
        
        echo -e "variant_chr\\tvariant_pos\\tREF\\tALT\\tProBound_bed\\tProBound_seq\\tProBound_score" > {output}
        cat {input.sequences} | grep "^>" | sed 's/>//g; s/::/\\t/g; s/_/\\t/g' \
        | cut -f1,2,4,5,6 | paste - <(cat {output}.temp.ProBound.txt | grep -v "http") >> {output}

        rm {output}.temp*
        """
