
# Count how many reads in fastq per sample
cat /g/furlong/project/68_F1_cisreg_ichip/data/Sample_tables/F1_samples_table.csv \
        | sed 's/,/\t/g' | cut -f1,7 | tail -n +2 > /scratch/forneris/temp/fastq_list.txt

echo -e "*sample_name\tn_reads" > ../../../analysis/mappability_filter/mapping_pipeline_WASP_logs/counts/fastq_files_read_counts.txt
while read line; 
        do 
        sample=$(echo $line | sed 's/\ /\t/g' | cut -f1); 
        file=$(echo $line | sed 's/\ /\t/g' | cut -f2); 
        echo $sample; zcat $file | wc -l | awk '{print ($1/4)"&"}'; 
done </scratch/forneris/temp/fastq_list.txt > /scratch/forneris/temp/fastq_counts.txt
cat /scratch/forneris/temp/fastq_counts.txt | tr "\n" "\t" | sed -e 's/&/\n/g; s/^[ \t]*//' | grep -e "\S" | awk '{print $1 "\t" $2}' \
        | sed 's/_/\t/g' | sed 's/h\t/\t/; s/\trep/\t/; s/^399/399_399/; s/^vgn\t/vgn_vgn\t/; s/vgn/vgn_/g' \
        | sed 's/vgn__/vgn_/g; s/input\ /input/g' | awk '{print $4 "." $2 "." $1 "_" $3 "\t" $5}' \
        >> ../../../analysis/mappability_filter/mapping_pipeline_WASP_logs/counts/fastq_files_read_counts.txt 



# Number of duplicate filtered reads without INDELs

cat  /scratch/forneris/temp/fastq_list.txt | grep -v Dsim | grep -v zld \
        | sed 's/_/\t/g; s/rep//; s/399/399_399/; s/vgn/vgn_/; s/h\t/\t/; s/input.*$/input/'         \
        | awk '{print $4 "." $2 "." $1 "_" $3 "\t" "/g/furlong/project/68_F1_cisreg_ichip/data/ChIPseq/Alignments/without_indels/rmdup/" $4 "." $2 "." $1 "_" $3  ".keep.merge.rmdup.sort.bam"}'         \
        | sed 's/vgn__/vgn_vgn_/g; s/vgn_399_399/vgn_399/g' > /scratch/forneris/temp/filtered_bam_WithoutINDELs.txt

conda activate samtools
echo -e "*sample_name\tduplicate_reads_filtered" > ../../../analysis/mappability_filter/mapping_pipeline_WASP_logs/counts/filtered_WithOutINDELs_bam_read_counts.txt
while read line; 
        do 
        sample=$(echo $line | sed 's/\ /\t/g' | cut -f1); 
        file=$(echo $line | sed 's/\ /\t/g' | cut -f2); 
        echo $sample; samtools flagstat $file | grep "0 mapped" | sed 's/\ /\t/g' | awk '{print $1"&"}'; 
done </scratch/forneris/temp/filtered_bam_WithoutINDELs.txt > /scratch/forneris/temp/filtered_bam_counts.txt
cat /scratch/forneris/temp/filtered_bam_counts.txt | tr "\n" "\t" | sed 's/&/\n/g; s/^[ \t]*//' | grep "\S" | sed 's/^\t*//' \
        >> ../../../analysis/mappability_filter/mapping_pipeline_WASP_logs/counts/filtered_WithOutINDELs_bam_read_counts.txt


# Number of duplicate filtered reads with INDELs

cat  /scratch/forneris/temp/fastq_list.txt | grep -v Dsim | grep -v zld \
        | sed 's/_/\t/g; s/rep//; s/399/399_399/; s/vgn/vgn_/; s/h\t/\t/; s/input.*$/input/'         \
        | awk '{print $4 "." $2 "." $1 "_" $3 "\t" "/g/furlong/project/68_F1_cisreg_ichip/data/ChIPseq/Alignments/with_indels/" $4 "." $2 "." $1 "_" $3  ".filtered.rmdup_withUMI.bam"}'         \
        | sed 's/vgn__/vgn_vgn_/g; s/vgn_399_399/vgn_399/g' > /scratch/forneris/temp/filtered_bam_WithINDELs.txt

conda activate samtools
echo -e "*sample_name\tduplicate_reads_filtered" > ../../../analysis/mappability_filter/mapping_pipeline_WASP_logs/counts/filtered_bam_read_counts.txt
while read line; 
        do 
        sample=$(echo $line | sed 's/\ /\t/g' | cut -f1); 
        file=$(echo $line | sed 's/\ /\t/g' | cut -f2); 
        echo $sample; samtools flagstat $file | grep "0 mapped" | sed 's/\ /\t/g' | awk '{print $1"&"}';  
done </scratch/forneris/temp/filtered_bam_WithINDELs.txt > /scratch/forneris/temp/filtered_bam_counts_WithINDELs.txt
cat /scratch/forneris/temp/filtered_bam_counts_WithINDELs.txt | tr "\n" "\t" | sed 's/&/\n/g; s/^[ \t]*//' | grep "\S" | sed 's/^\t*//' \
        >> ../../../analysis/mappability_filter/mapping_pipeline_WASP_logs/counts/filtered_bam_read_counts.txt


# WASP first round number of reads
cat  /scratch/forneris/temp/fastq_list.txt | grep -v Dsim | grep -v zld \
        | sed 's/_/\t/g; s/rep//; s/399/399_399/; s/vgn/vgn_/; s/h\t/\t/; s/input.*$/input/'         \
        | awk '{print $4 "." $2 "." $1 "_" $3 "\t" "/g/furlong/project/68_F1_cisreg_ichip/analysis/mappability_filter/mapping_pipeline_WASP_logs/" $4 "." $2 "." $1 "_" $3  ".first_alignment_log.txt"}'         \
        | sed 's/vgn__/vgn_vgn_/g; s/vgn_399_399/vgn_399/g' > /scratch/forneris/temp/WASP_first_round_filenames.txt


echo -e "*sample_name\tmapped_reads\tpassed_MAPQ_filter\tpassed_pre-WASP_QC" > ../../../analysis/mappability_filter/mapping_pipeline_WASP_logs/counts/WASP_first_round_read_counts.txt
while read line; 
        do 
        sample=$(echo $line | sed 's/\ /\t/g' | cut -f1); 
        file=$(echo $line | sed 's/\ /\t/g' | cut -f2); 
        echo $sample;         
        cat <(cat $file | grep -e 'total reads\|secondary\|  unmapped' | sed 's/: /\t/g' | cut -f2 | tr "\n" "\t" | awk '{print $1 -$2 -$3}') \
        <(cat $file | grep -e 'passed' | sed 's/: /\t/g' | cut -f2) \
        <(cat $file | grep -e 'none\|to remap' | sed 's/: /\t/g' | cut -f2 | tr "\n" "\t" | awk '{print $1 + $2}') | tr "\n" "\t"; 
        echo "&";
done </scratch/forneris/temp/WASP_first_round_filenames.txt > /scratch/forneris/temp/WASP_first_round_read_counts.txt
cat /scratch/forneris/temp/WASP_first_round_read_counts.txt | tr "\n" "\t" | sed 's/&/\n/g; s/^[ \t]*//' \
        | grep "\S" | sed 's/^[ \t]+//g' | awk '{print $1 "\t" $2 "\t" $3 "\t" $4}' | awk 'NF==4' \
        >> ../../../analysis/mappability_filter/mapping_pipeline_WASP_logs/counts/WASP_first_round_read_counts.txt


# WASP second round number of reads

cat /scratch/forneris/temp/WASP_first_round_filenames.txt | sed 's/first_alignment/second_alignment/g' > /scratch/forneris/temp/WASP_second_round_filenames.txt
echo -e "*sample_name\tmappability_filter" > ../../../analysis/mappability_filter/mapping_pipeline_WASP_logs/counts/WASP_second_round_read_counts.txt
while read line; 
        do 
        sample=$(echo $line | sed 's/\ /\t/g' | cut -f1);
        file_first=$(echo $line | sed 's/\ /\t/g' | cut -f2 | sed 's/2_map2/1_map1/; s/.second_alignment_log.txt/.first_alignment_log.txt/')
        file_second=$(echo $line | sed 's/\ /\t/g' | cut -f2); 
        echo $sample; 
        cat $file_first $file_second | grep -e 'none\|kept reads' | sed 's/: /\t/g' | cut -f2 | tr "\n" "\t" | awk '{print $1 + $2}';
        echo "&";
done </scratch/forneris/temp/WASP_second_round_filenames.txt > /scratch/forneris/temp/WASP_second_round_read_counts.txt
cat /scratch/forneris/temp/WASP_second_round_read_counts.txt | tr "\n" "\t" | sed 's/&/\n/g; s/^[ \t]*//' \
        | grep "\S" | sed 's/^[ \t]+//g' | awk '{print $1 "\t" $2 "\t" $3 "\t" $4}' | awk 'NF==2' \
        >> ../../../analysis/mappability_filter/mapping_pipeline_WASP_logs/counts/WASP_second_round_read_counts.txt


# Join results for filtered reads in withINDELs pipeline

join -1 1 -2 1  <(sort -k1,1 ../../../analysis/mappability_filter/mapping_pipeline_WASP_logs/counts/fastq_files_read_counts.txt) \
        <(sort -k1,1 ../../../analysis/mappability_filter/mapping_pipeline_WASP_logs/counts/WASP_first_round_read_counts.txt) \
        | sed 's/\ /\t/g' | sort -k1,1 | join -1 1 -2 1 - \
        <(sort -k1,1 ../../../analysis/mappability_filter/mapping_pipeline_WASP_logs/counts/WASP_second_round_read_counts.txt) \
        | sed 's/\ /\t/g' | sort -k1,1 | join -1 1 -2 1 - \
        <(sort -k1,1 ../../../analysis/mappability_filter/mapping_pipeline_WASP_logs/counts/filtered_bam_read_counts.txt) \
        | sed 's/\ /\t/g' | sort -k1,1 | sed 's/.sample_name/sample_name/g' \
        > ../../../analysis/mappability_filter/mapping_pipeline_WASP_logs/counts/WASP_filtered_reads_whole_pipeline_withINDELs.txt

echo -e "sample_name\tn_reads\tmapped_reads\tpassed_MAPQ_filter\tpassed_pre-WASP_QC\tmappability_filter\tduplicate_reads_filtered\tcondition\tantibody\tcross\trep" \
        > ../../../analysis/mappability_filter/mapping_pipeline_WASP_logs/counts/WASP_filtered_reads_whole_pipeline_withINDELs_samples.txt
cat ../../../analysis/mappability_filter/mapping_pipeline_WASP_logs/counts/WASP_filtered_reads_whole_pipeline_withINDELs.txt | tail -n +2 \
        | paste - <(cut -f1 ../../../analysis/mappability_filter/mapping_pipeline_WASP_logs/counts/WASP_filtered_reads_whole_pipeline_withINDELs.txt \
        | tail -n +2 | sed 's/\./\t/g' | cut -f1,2 | awk '{print $1 "." $2}') \
        | paste - <(cut -f1 ../../../analysis/mappability_filter/mapping_pipeline_WASP_logs/counts/WASP_filtered_reads_whole_pipeline_withINDELs.txt \
        | tail -n +2 | sed 's/\./\t/g' | cut -f1) \
        | paste - <(cut -f1 ../../../analysis/mappability_filter/mapping_pipeline_WASP_logs/counts/WASP_filtered_reads_whole_pipeline_withINDELs.txt \
        | tail -n +2 | sed 's/\./\t/g' | cut -f3 | sed 's/_[12]$//') \
        | paste - <(cut -f1 ../../../analysis/mappability_filter/mapping_pipeline_WASP_logs/counts/WASP_filtered_reads_whole_pipeline_withINDELs.txt \
        | tail -n +2 | sed 's/\./\t/g' | cut -f3 | sed 's/_/\t/g' | cut -f3) \
        >> ../../../analysis/mappability_filter/mapping_pipeline_WASP_logs/counts/WASP_filtered_reads_whole_pipeline_withINDELs_samples.txt


# Compare reads at end of withINDEL and withoutINDEL pipelines

echo -e "sample_name\twith_INDELs\twithout_INDELs\tcondition\tantibody\tcross\trep" > ../../../analysis/mappability_filter/mapping_pipeline_WASP_logs/counts/duplicated_reads_filtered_with_VS_without_INDELs.txt
join -1 1 -2 1 <(sort -k1,1 ../../../analysis/mappability_filter/mapping_pipeline_WASP_logs/counts/filtered_bam_read_counts.txt | grep -v "sample_name") \
        <(sort -k1,1 ../../../analysis/mappability_filter/mapping_pipeline_WASP_logs/counts/filtered_WithOutINDELs_bam_read_counts.txt | grep -v "sample_name") \
        | sed 's/\ /\t/g' | sort -k1,1  \
        | paste - <(sort -k1,1  ../../../analysis/mappability_filter/mapping_pipeline_WASP_logs/counts/filtered_bam_read_counts.txt \
        | tail -n +2 | cut -f1 | sed 's/\./\t/g' | cut -f1,2 | awk '{print $1 "." $2}') \
        | paste - <(sort -k1,1  ../../../analysis/mappability_filter/mapping_pipeline_WASP_logs/counts/filtered_bam_read_counts.txt \
        | tail -n +2 | cut -f1 | sed 's/\./\t/g' | cut -f1) \
        | paste - <(sort -k1,1  ../../../analysis/mappability_filter/mapping_pipeline_WASP_logs/counts/filtered_bam_read_counts.txt \
        | tail -n +2 | cut -f1 | sed 's/\./\t/g' | cut -f3 | sed 's/_[12]$//') \
        | paste - <(sort -k1,1  ../../../analysis/mappability_filter/mapping_pipeline_WASP_logs/counts/filtered_bam_read_counts.txt \
        | tail -n +2 | cut -f1 | sed 's/\./\t/g' | cut -f3 | sed 's/_/\t/g' | cut -f3) \
        >> ../../../analysis/mappability_filter/mapping_pipeline_WASP_logs/counts/duplicated_reads_filtered_with_VS_without_INDELs.txt



# Filtering within WASP pipeline

cat  /scratch/forneris/temp/fastq_list.txt | grep -v Dsim | grep -v zld \
        | sed 's/_/\t/g; s/rep//; s/399/399_399/; s/vgn/vgn_/; s/h\t/\t/; s/input.*$/input/'         \
        | awk '{print $4 "." $2 "." $1 "_" $3 "\t" "/g/furlong/project/68_F1_cisreg_ichip/analysis/mappability_filter/mapping_pipeline_WASP_logs/" $4 "." $2 "." $1 "_" $3  ".first_alignment_log.txt"}'         \
        | sed 's/vgn__/vgn_vgn_/g; s/vgn_399_399/vgn_399/g' > /scratch/forneris/temp/WASP_first_round_filenames.txt
cat /scratch/forneris/temp/WASP_first_round_filenames.txt | sed 's/first_alignment/second_alignment/g' > /scratch/forneris/temp/WASP_second_round_filenames.txt

echo -e "sample_name\tTot_reads\tINDEL_reads\tMappability_filtered\tAllelic_combinations\tImproper_mapping\tcondition\tantibody\tcross\trep" > ../../../analysis/mappability_filter/mapping_pipeline_WASP_logs/counts/filtering_within_WASP_counts.txt
while read line; 
        do 
        sample=$(echo $line | sed 's/\ /\t/g' | cut -f1);
        file_first=$(echo $line | sed 's/\ /\t/g' | cut -f2 | sed 's/2_map2/1_map1/; s/.second_alignment_log.txt/.first_alignment_log.txt/')
        file_second=$(echo $line | sed 's/\ /\t/g' | cut -f2); 
        echo $sample; 
        cat $file_first $file_second \
        | grep -e 'none\|biallelic:\|  snv:\|mixed:\|excess reads\|to remap\|  unmapped:\|low mapping quality:\|mismapped \|kept reads' \
        | tail -n +3 | sed 's/: /\t/g' | cut -f2 | tr "\n" "\t" | awk '{print $1 + $6 "\t" $2 "\t" $10 "\t" $5 "\t" ($7+$8+$9)}' 
        echo "&";
done </scratch/forneris/temp/WASP_second_round_filenames.txt > /scratch/forneris/temp/filtering_within_WASP_counts.txt

cat /scratch/forneris/temp/filtering_within_WASP_counts.txt | tr "\n" "\t" | sed 's/&/\n/g; s/^[ \t]*//' \
        | grep "\S" | sed 's/^[ \t]+//g; s/\*//' | awk '{print $1 "\t" $2 "\t" $3 "\t" $4 "\t" $5 "\t" $6}' | awk 'NF==6' | sort -k1,1 \
        > /scratch/forneris/temp/filtering_within_WASP_counts_reformat.txt
          
cat /scratch/forneris/temp/filtering_within_WASP_counts_reformat.txt \
        | paste - <(cat /scratch/forneris/temp/filtering_within_WASP_counts_reformat.txt \
        | cut -f1 | sed 's/\./\t/g' | cut -f1,2 | awk '{print $1 "." $2}') \
        | paste - <(cat /scratch/forneris/temp/filtering_within_WASP_counts_reformat.txt \
        | cut -f1 | sed 's/\./\t/g' | cut -f1 | sed 's/\./\t/g' | cut -f1) \
        | paste - <(cat /scratch/forneris/temp/filtering_within_WASP_counts_reformat.txt \
        | cut -f1 | sed 's/\./\t/g' | cut -f3 | sed 's/_[12]$//') \
        | paste - <(cat /scratch/forneris/temp/filtering_within_WASP_counts_reformat.txt \
        | cut -f1 | sed 's/\./\t/g' | cut -f3 | sed 's/_/\t/g' | cut -f3) \
        >> ../../../analysis/mappability_filter/mapping_pipeline_WASP_logs/counts/filtering_within_WASP_counts.txt


# Number of significant associations with VS without INDELs

        #with INDELs

        #Without INDELs
        #../../../analysis/ChIPseq/CHT/without_indels/stringent/bin/1012/width_500/adjust_hap_read_counts/cht_results_as.txt


# Effect sizes with VS without INDELs




