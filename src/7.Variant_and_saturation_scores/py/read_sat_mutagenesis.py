import h5py
import argparse
import pandas as pd
import numpy as np


usage = 'usage: %prog [options] <params_file> <model_file> <bed_file>'
parser = argparse.ArgumentParser(description=usage)
parser.add_argument('-i', dest='input_h5',
      default=None, type=str,
      help='input h5 file with sturation scores from basenji_sat_bed.py')
parser.add_argument('-s', dest='sample_index',
      default=None, type=int,
      help='sample ID. This sample\'s scores will be normalized and saved to output')
parser.add_argument('-o', dest='out_file',
      default=None, type=str,
      help='Output files root name.')
args = parser.parse_args()


def replace_negatives(x):
    if x < 0:
        return 0
    else:
        return x



hf = h5py.File(args.input_h5, 'r')

# Get normalized differences per track (for 1st track)
scores = pd.DataFrame(hf['sum'][()][0, :, :, args.sample_index]).T
avg = scores.abs().mean().mean()
norm_scores = scores / avg
ref_scores = norm_scores[pd.DataFrame(hf['seqs'][()][0,:,:]).T].sum()
diff_scores = norm_scores - ref_scores

# Compute importance scores
importance_scores_pos = diff_scores.applymap(replace_negatives).sum() 
importance_scores_neg = (diff_scores * -1).applymap(replace_negatives).sum() 

# Get reference sequence
seq_bool = pd.DataFrame(hf['seqs'][()][0, :, :])
ref_seq = "".join([str(np.flatnonzero(l)[0]).replace("0", "A").replace("1", "C").replace("2", "G").replace("3", "T") \
    for i, l in seq_bool.iterrows()])

diff_scores_filename = args.out_file + "_NormalizedDifferenceScores.txt"
importance_scores_pos_filename = args.out_file + "_ImportanceScoresPos.txt"
importance_scores_neg_filename = args.out_file + "_ImportanceScoresNeg.txt"
ref_sequence_filename = args.out_file + "_ReferenceSequence.txt"

diff_scores.index = ["A", "C", "G", "T"]
diff_scores.T.to_csv(diff_scores_filename, index=True, sep="\t")
importance_scores_pos.to_csv(importance_scores_pos_filename, index=True, sep="\t")
importance_scores_neg.to_csv(importance_scores_neg_filename, index=True, sep="\t")
pd.Series(ref_seq).to_csv(ref_sequence_filename, index=False, header=False, sep="\t")
