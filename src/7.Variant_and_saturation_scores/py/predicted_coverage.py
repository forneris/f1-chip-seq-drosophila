# Import libraries
import h5py
import pandas as pd
import argparse


# Read arguments
usage = 'usage: %prog -p <predict_file> -s <sample_index> -o <out_file>'
parser = argparse.ArgumentParser(description=usage)
parser.add_argument('-p', dest='predict_file',
      default=None, type=str,
      help='Basenji predict output file')
parser.add_argument('-s', dest='sample_index',
      default=None, type=int,
      help='Sample index. Index of sample in the training table')
parser.add_argument('-o', dest='out_file',
      default=None, type=str,
      help='Output file name.')
args = parser.parse_args()


# Read inputs
f = h5py.File(args.predict_file)
test_preds = f['preds'][:]
important_tracks_index = args.sample_index
bin_size = int((f['end'][0] - f['start'][0]) / test_preds[0].shape[0])


# Run through bins and create bedgraph file
colnames = ['chrom', 'start', 'end', 'signal']
full_begraph = pd.DataFrame(columns=colnames)

for b in range(0, f['start'].shape[0]):
    print(b)
    chrom = f['chrom'][b].decode('UTF-8')
    start = f['start'][b]
    end = f['end'][b]
    bin_signal = list(test_preds[:, :, important_tracks_index])[b]
    for sub_bin in range(0, len(bin_signal)):
        sub_bin_start = start + sub_bin * bin_size
        sub_bin_end = sub_bin_start + bin_size
        bin_signal_value = bin_signal[sub_bin]
        current_bin = pd.DataFrame([chrom, sub_bin_start, sub_bin_end, bin_signal_value]).T
        current_bin.columns = colnames
        full_begraph = pd.concat([full_begraph, current_bin])


# Merge bins from differet prediction windows and save output
full_begraph = full_begraph.sort_values(['chrom', 'start'], ascending = [True, True])
full_merged_bedgraph = full_begraph.groupby(['chrom', 'start', 'end'])['signal'].sum().reset_index()
full_merged_bedgraph = full_merged_bedgraph.sort_values(['chrom', 'start'], ascending = [True, True])
full_merged_bedgraph.to_csv(args.out_file, sep="\t", header=False, index=False) 
full_begraph.to_csv(args.out_file, sep="\t", header=False, index=False) 
