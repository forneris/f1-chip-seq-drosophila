

global:
  projectdir: /g/furlong/project/68_F1_cisreg_ichip
  #scratchdir_old: /scratch/sigalova/68_F1_cisreg_ichip
  scratchdir_old: /scratch/forneris/F1_project/mapping_old
  #scratchdir: /scratch/sigalova/data
  scratchdir: /scratch/forneris/F1_project/mapping
  #tmpdir: /tmp/sigalova
  tmpdir: /scratch/forneris/temp

project_structure:
  datadir: data
  analysisdir: analysis
  configdir: config
  logdir: log
  envdir: env
  condaenvdir:
    configs: env/conda-env/configs
    built: env/conda-env/built
  tmpdir: tmp
  srcdir: src
  utils: src/utils

output_dirs:
  figures_main: /g/furlong/project/68_F1_cisreg_ichip/Manuscript/Figures/main/
  figures_suppl: /g/furlong/project/68_F1_cisreg_ichip/Manuscript/Figures/suppl
  tables_suppl: /g/furlong/project/68_F1_cisreg_ichip/Manuscript/Tables/suppl

tools:
  by_path:
    bwa: /g/funcgen/bin/bwa-0.7.15
    samtools: /g/funcgen/bin/samtools-1.9
    picard: /g/funcgen/bin/picard-2.16.0
    je:
      v1: /g/funcgen/bin/je-1.2
      v2: /g/funcgen/bin/je_2.0.3
    embase: /g/funcgen/bin/embase
    bedtools: /g/funcgen/bin/bedtools-2.26.0
    skewer: /g/funcgen/bin/skewer-0.2.2
    seqtk: /g/easybuild/x86_64/CentOS/7/nehalem/software/seqtk/1.2-foss-2016b/seqtk
    wasp: /g/furlong/sigalova/software/WASP
    wasp_indels: /g/furlong/sigalova/software/wasp_indels
    wasp_indels_extended: /g/furlong/sigalova/software/wasp_haplotypes
    ChIPnexus_scripts: /g/furlong/sigalova/software/chipnexus-processing-scripts
    bowtie2: /g/funcgen/bin/bowtie2-2.3.4.1
    macs2: /g/funcgen/bin/macs2-2.1.2.1
    bamCoverage: /g/funcgen/bin/bamCoverage-3.1.3
    bamCompare: /g/funcgen/bin/bamCompare-3.1.3
    bigwigCompare: /g/funcgen/bin/bigwigCompare-3.1.3
    plotCorrelation: /g/funcgen/bin/plotCorrelation-3.1.3
    multiBamSummary: /g/funcgen/bin/multiBamSummary-3.1.3
    computeMatrix: /g/funcgen/bin/computeMatrix-2.5.1
    plotHeatmap: /g/funcgen/bin/plotHeatmap-2.5.1
    plotProfile: /g/funcgen/bin/plotProfile-2.5.1
    bcftools: /g/furlong/sigalova/software/bcftools/bcftools
    IDR: /g/furlong/sigalova/software/anaconda3/bin/idr
    runIDR: /g/furlong/sigalova/software/06_run_IDR_WF-paired_parallel_v1.2.sh
    python:
      py27: /usr/bin/python2.7
      py35: /g/furlong/sigalova/software/conda/bin/python3.5
    Rscript:
      v3.5.1: /g/funcgen/bin/Rscript-3.5.1
      v4.0: /g/funcgen/bin/Rscript-4.0.0
  by_module:
    bwa: BWA/0.7.15-foss-2016b
    samtools: SAMtools/1.9-foss-2018b
    seqtk: seqtk/1.2-foss-2016b # r94
    skewer: skewer/0.2.2-foss-2016b
    je: Je/1.2-Java-1.8.0_112
    macs2: MACS2/2.2.7.1-foss-2020a-Python-3.8.2
    bedtools: BEDTools/2.26.0-foss-2016b
    meme: MEME/4.11.3-foss-2016b
    perl: Perl/5.24.1-foss-2016b



pipelines:

  general:
    VCF_set:
      mappability_filter: lenient
      as_counts: stringent

  1_preprocess_fastq:
    software_params:
      je_clip: LEN=8 BPOS=BOTH ADD=TRUE SAME_HEADERS=TRUE XT=1 ZT=0 RCHAR=':'
      skewer: -m pe -z -x 'NNNNNNNNNAGATCGGAAGAGCACACGTCTGAACTCCAGTCACNNNNNNNNATCTCGTATGCCGTCTTCTGCTTG' -y 'NNNNNNNNNAGATCGGAAGAGCGTCGTGTAGGGAAAGAGTGTAGATCTCGGTGGTCGCCGTATCATT'

  2_mappability_filter:
    chrom_list: chr2L chr2R chr3L chr3R chr4 chrM chrX chrY
    software_params:
      bwa_aln: -n 5 # old version of pipeline
      bwa_sampe: -s # old version of pipeline
      samtools_filter: -f 2 -F 4 -q 10 # old version of pipeline
      minMAPQ: 10
      max_vars: 8 # The maximum number of variants allowed to overlap a read before discarding the read
      max_seqs: 256 # maximum number of allele flipped reads (or read pairs) to consider remapping.

  3_call_peaks:
      macs2_standalone: -q 0.05 --call-summits --format BAMPE  --gsize 142573017
      macs2_relaxed4IDR: -p 0.5 --format BAMPE  --gsize 142573017
      consensus_set:
        input_type: merged_input
        idr_thres: 0.01
        num_ind: 3



  4_CHT:
      peaks_resize_width: 5000 # regions around peaks center to asign variants to peaks
      peaks_resize_width_extended: 100000 # regions around peaks center to asign variants to peaks - for identifying distant variants (with wasp_haplotypes branch)
      target_region_size: 1000 # regions around SNPs to consider (if peaks not provided)
      min_read_count: 400 # minimum number of mapped reads in target region (summed across individuals)
      min_as_count: 50 #  # minimum number of allele-specific reads (combined across individuals) required to perform combined test on a region
      min_het_count: 1
      min_minor_allele_count: 1
      chrom_list: chr2L chr2R chr3L chr3R chr4 chrM chrX chrY

  6_ChIPexo:
    software_params:
      je_clip: LEN=9 BPOS=READ_1 ADD=TRUE SAME_HEADERS=TRUE
      skewer: -m pe -z -x 'NNNNNNNNNAGATCGGAAGAGCACACGTCTGGATCCACGACGCTCTTCC' -y 'NNNNNNNNNAGATCGGAAGAGCGTCGTGTAGGGAAAGAGTGTAGATCTCGGTGGTCGCCGTATCATT'
      samtools_filter: -f 2 -q 10 # only reads mapped with proper pair, MAPQ 10

  old_version:
    3_allele_specific_counts:
      vcf_params:
        set: stringent
        chrom_list: chr2L chr2R chr3L chr3R chr4 chrM chrX chrY



results:
  CHT:
    main_directories:
      with_indels: /g/furlong/project/68_F1_cisreg_ichip/data/ChIPseq/CHT/with_indels/
  counts:
    by_region: /g//furlong/project/68_F1_cisreg_ichip/data/ChIPseq/AS_counts/with_indels/2_target_regions/by_peaks_width5000/merged_input_idr0.01_ind3

data:

  genome:
    dm6:
      fasta: /g/furlong/genome/D.melanogaster/Dm6/fasta/dm6.UCSC.noMask.fa
      fasta_by_chr: /g/furlong/project/68_F1_cisreg_ichip/data/Genome/by_chromosome
      chrom_sizes: /g/furlong/genome/D.melanogaster/Dm6/chrsizes/dm6.ucsc.chrom.sizes
      index_bwa: /g/furlong/genome/D.melanogaster/Dm6/indexes/bwa//dm6.UCSC.noMask.fa
      gff_problemcasefilter: /g/furlong/genome/D.melanogaster/Dm6/6.13/gff/dmel-all-filtered-r6.13-problemcasefilter.gff.gz
      gff_full: /g/furlong/genome/D.melanogaster/Dm6/6.13/gtf/dmel-all-r6.13.gtf.gz
      gff_genes: /g/furlong/users/sigalova/data/genome/gff_genes.v6.13_chromUCSF.gff
      genes_annot: /g/furlong/project/62_expression_variation/data/feature_tables/genes_v6.13_annotated.txt
      gff_transcripts: /g/furlong/users/sigalova/data/genome/gff_transcripts.v6.13_chromUCSF.gff
      gff_introns: /g/furlong/users/sigalova/data/genome/gff_introns.v6.13_chromUCSF.gff
      gff_exons: /g/furlong/users/sigalova/data/genome/gff_exons.v6.13_chromUCSF.gff
      utr3_coordinates: /g/furlong/project/55_polyA_sites_resource/data/peaks/reproducing_polyA_paper_peaks/peaks.coverage.drop.filtered.gene.association.UTR.length.bed
    chain_dm3_to_dm6: /g/furlong/genome/chain_files/dm3ToDm6.over.chain
    genome_size:
      dm6: 142573017

  samples:
    sample_table: /g/furlong/project/68_F1_cisreg_ichip/data/Sample_tables/F1_samples_table.csv
    lines_data: /g/furlong/project/68_F1_cisreg_ichip/data/Sample_tables/line_specific_data.txt
    individual_ids: /g/furlong/project/68_F1_cisreg_ichip/data/Sample_tables/individual_ids.txt
    individual_ids_1012h: /g/furlong/project/68_F1_cisreg_ichip/data/Sample_tables/individual_ids_1012h.txt
    individual_ids_input: /g/furlong/project/68_F1_cisreg_ichip/data/Sample_tables/individual_ids_input.txt
    chip_exo_samples: /g/furlong/project/68_F1_cisreg_ichip/data/ChIPexo/ChIPexo_samples_info.csv

  adapter_sequences: /g/furlong/project/68_F1_cisreg_ichip/data/sequencing/adapter_sequences.txt

  VCF:
    vcf_dir: /g/furlong/genome/D.melanogaster/Dm6/F1_lines/vcf
    stringent: F1_haplotype_joint_call_GATK_stringent.vcf.gz
    lenient: F1_haplotype_joint_call_GATK_lenient.vcf.gz

  phased_VCF:
    stringent_with_gl: /g/furlong/project/68_F1_cisreg_ichip/data/Genotypes/phased_VCF/haplotypes.vcf.gz
    stringent_with_gl_old: /g/furlong/project/68_F1_cisreg_ichip/data/Genotypes/old/phased_VCF_stringent/with_GL/haplotypes.missing_filt.vcf.gz

  SNP_H5:
    hdf5_stringent_dir_old: /g/furlong/project/68_F1_cisreg_ichip/data/phased_VCF_stringent/HDF5
    hdf5_stringent_dir: /g/furlong/project/68_F1_cisreg_ichip/data/phased_VCF_stringent/with_GL/HDF5

  motif_databases:
    CISBP:  /g/furlong/sigalova/data/motif_databases/meme_motif_databases_28Oct2019/CIS-BP/Drosophila_melanogaster.meme
    CISBP_motifs_info: /g/furlong/sigalova/data//motif_databases/CISBP/Drosophila_melanogaster_2019_03_05/TF_Information_all_motifs_plus.ids_updated.csv
    selected_motifs: /g/furlong/project/68_F1_cisreg_ichip/data/Motifs/selected_motifs.meme
    combined_motifs: /g/furlong/project/68_F1_cisreg_ichip/data/Motifs/combined/alternative_pfms.meme
    denovo_motifs: /g/furlong/project/68_F1_cisreg_ichip/data/Motifs/denovo_pwms/denovo_motifs_from_peaks.meme
    functional_pfm_motifs: /g/furlong/project/68_F1_cisreg_ichip/data/Motifs/denovo_pwms/functional_pfms.meme
    individual_pwms:
      known: /g/furlong/project/68_F1_cisreg_ichip/data/Motifs/known_pwms
      denovo: /g/furlong/project/68_F1_cisreg_ichip/data/Motifs/denovo_pwms

  scanned_motifs:
    dir: /g/furlong/project/68_F1_cisreg_ichip/analysis/ChIPseq/Motif_analysis/with_indels/merged_input_idr0.01_ind3/

  peaks:
    consensus_peaksets: /g/furlong/project/68_F1_cisreg_ichip/data/ChIPseq/Peaks/with_indels/merged_input/Consensus_peaksets

  signal:
    chip_seq_tracks: /g/furlong/project/68_F1_cisreg_ichip/data/ChIPseq/Signal/with_indels/rpgc/
    chip_nexus_tracks: /g/furlong/project/68_F1_cisreg_ichip/data/ChIPexo/signal_v3

  DHS:
    dm6_liftover_annot: /g/furlong/project/62_expression_variation/data/feature_tables/DHS-clusters_liftOver_dm6_annotated_uf.txt
    dm6_liftover_with_sd: /g/furlong/sigalova/data/DHS_James/dhs_with_sd_dm6_liftOver.gff3

  CHT:
    CHT_dir: /g/furlong/project/68_F1_cisreg_ichip/data/ChIPseq/CHT/with_indels/by_peaks_width5000/merged_input_idr0.01_ind3

  Sequence_conservation:
    phasCons: /g/furlong/sigalova/data/sequence_conservation_score/dm6.27way.phastCons.bw
    phyloP: /g/furlong/sigalova/data/sequence_conservation_score/dm6.phyloP27way.bw

  external_data:
    Promoter_shape: /g/furlong/sigalova/data/Schor2017_CAGE/liftover/ng.3791-S7_dm6.gff3
    ChIP:
      Mef2:
        6_8h: /g/furlong/DataWarehouse/Data/datawarehouse/furlong_data/dmelanogaster/microarrays/ChIP-chip/whole_embryo//Mef2/6-8h//peaks/dm6/Mef2_6-8h_ChIP-chip_peaks-summits_200bp_lifted_Zinzen2009.gff
        10_12h: /g/furlong/DataWarehouse/Data/datawarehouse/furlong_data/dmelanogaster/microarrays/ChIP-chip/whole_embryo//Mef2/10-12h//peaks/dm6/Mef2_10-12h_ChIP-chip_peaks-summits_200bp_lifted_Zinzen2009.gff
      Bin:
        6_8h: /g/furlong/DataWarehouse/Data/datawarehouse/furlong_data/dmelanogaster/microarrays/ChIP-chip/whole_embryo//Biniou/6-8h//peaks/dm6/Bin_6-8h_ChIP-chip_peaks-summits_200bp_lifted_Zinzen2009.gff
        10_12h: /g/furlong/DataWarehouse/Data/datawarehouse/furlong_data/dmelanogaster/microarrays/ChIP-chip/whole_embryo//Biniou/10-12h//peaks/dm6/Bin_10-12h_ChIP-chip_peaks-summits_200bp_lifted_Zinzen2009.gff
      Twi:
        2_4h: /g/furlong/DataWarehouse/Data/datawarehouse/furlong_data/dmelanogaster/microarrays/ChIP-chip/whole_embryo/Twist/2-4h/peaks/dm6/Twi_2-4h_ChIP-chip_peaks-summits_200bp_lifted_Zinzen2009.gff
      CTCF:
        embryo:
          ctcf_c: /g/furlong/project/59_ChIP-CRM-insulators/data/chip_chip/GSM409069_CTCF_C.dm6.bed
          ctcf_n: /g/furlong/project/59_ChIP-CRM-insulators/data/chip_chip/GSM409070_CTCF_N.dm6.bed
      Zld:
        stage8:
          dm3: /g/furlong/project/68_F1_cisreg_ichip/data/external_chip/zld/Harrison2011_zld_stage8.bed
          dm6: /g/furlong/project/68_F1_cisreg_ichip/data/external_chip/zld/Harrison2011_zld_stage8_liftOver_dm6.bed
