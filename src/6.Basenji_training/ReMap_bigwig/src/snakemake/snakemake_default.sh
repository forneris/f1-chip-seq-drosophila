#!/usr/bin/bash

#Params

SNAKEMAKE=snakemake
MAX_N_JOBS=1000
CLUSTER_CONFIG_FILE_NAME=cluster.json
LATENCY_WAIT=10
RESTART_TIMES=1

# Command

$SNAKEMAKE -j $MAX_N_JOBS --cluster-config $CLUSTER_CONFIG_FILE_NAME \
	--cluster "{cluster.sbatch} -p {cluster.partition} --cpus-per-task {cluster.n} --mem {cluster.mem} {cluster.moreoptions}" \
	--keep-going --rerun-incomplete --latency-wait $LATENCY_WAIT --restart-times $RESTART_TIMES --use-conda
