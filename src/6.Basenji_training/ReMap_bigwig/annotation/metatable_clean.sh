# Create clean biotype field

paste <(cat remap2022_metadata_dm6_v1_0.tsv | cut -f5 | sed 's/:[A-Za-z0-9]*//g; s/-FBgn.*//g; \
	s/Kc167/Kc/g; s/S2-DRSC/S2/g; s/S2R-plus/S2/g; s/Schneider-2/S2/g; \
	s/biotype/biotype_clean/g; s/third-instar/third_instar_larva/g; \
	s/second-instar/second_instar_larva/; s/ovarian-somatic-cell/ovarian_somatic_cell/g; \
	s/mushroom-body/mushroom_body/') remap2022_metadata_dm6_v1_0.tsv \
	| awk '{OFS="\t"} {print $2, $3, $4, $5, $6, $1, $7, $8, $9}' > remap2022_metadata_dm6_v1_0_clean.tsv

paste remap2022_metadata_dm6_v1_0_clean.tsv <(cat <(echo "tissue") <(cat remap2022_metadata_dm6_v1_0.tsv | cut -f6 | sed 's/,/\t/g' | awk '{print $0 "\t%"}' | grep -of <(cat tissue_terms.txt <(echo "%")) | sed 's/\t/,/g' | tr "\n" "%" | sed 's/%%/\n/g' | sed 's/%//g' | tail -n +2 | sed 's/eye-antennal-disc/eye-antenna-imaginal-disc/g; s/eye-antennal-imaginal/eye-antenna-imaginal-disc/g; s/eye-antennal-imaginal-disc/eye-antenna-imaginal-disc/g; s/eye-antennal-disc/eye-antenna-imaginal-disc/g; s/eye-disk/eye-disc/g; s/imaginal-disk/imaginal-disc/g; s/imaginal-discs-and-brains/imaginal-disc,brain/g; s/salivary-glands/salivary-gland/g; s/wing-disc/wing-imaginal-disc/g')) | head

