## Contextualising transcription factor binding during embryogenesis using natural sequence variation

<div align="justify">

This repository contains code to reproduce the results of our paper **Integrating genetic variation with deep learning provides context for variants impacting transcription factor binding during embryogenesis** by Olga Sigalova*, Mattia Forneris*, Frosina Stojanovska, Bingqing Zhao, Rebecca R. Viales, Adam Rabinowitz, Fayrouz Hamal, Benoît Ballester, [Judith Zaugg](https://www.zaugg.embl.de/), and [Eileen Furlong](http://furlonglab.embl.de/), *Genome Research*, 2025, [doi: 10.1101/gr.279652.124](https://doi.org/10.1101/gr.279652.124)

<div align="justify">

Understanding how genetic variation impacts transcription factor (TF) binding remains a major challenge, limiting our ability to model disease-associated variants.  Here, we used a highly controlled system of F1 crosses with extensive genetic diversity to profile allele-specific binding of four TFs at several time-points during Drosophila embryogenesis.  Using a combined haplotype test, we identified 9-18% of TF bound regions impacted by genetic variation even for essential regulators.  By expanding WASP (a tool for allele-specific read mapping) to examine INDELs, we increased detection of allelic imbalanced peaks by 30-50%.  This fine-grained ‘mutagenesis’ could reconstruct functionalized binding motifs for all factors.  To prioritise causal variants, we trained a convolutional neural network (Basenji) to accurately predict binding from DNA sequence.  The model could also predict measured allelic imbalance for strong effect variants, providing a mechanistic interpretation for how the variant impacts binding.  This revealed unexpected relationships between TFs, including potential cooperative pairs, and mechanisms of tissue specific recruitment of the ubiquitous factor CTCF.

<div align="justify">
<div align="justify">

## Installation
Clone this repo:

```bash
$ git clone https://git.embl.de/grp-furlong/shared/drosophila-f1-chip-seq.git YOUR_PROJECT_NAME
$ cd YOUR_PROJECT_NAME
```

<div align="justify">

## Installation requirements

* Make sure your are running bash version > 4
* Make sure [snakemake](https://snakemake.readthedocs.io/en/stable/getting_started/installation.html) is installed 
* Most pipelines depend on [conda environments](https://docs.conda.io/projects/conda/en/latest/index.html)
* You can install Basenji from the [GitHub page](https://github.com/calico/basenji)

<div align="justify">

## Generating the Results

* You can check /src pipelines ordered 1. to 7. to generate all the analysed results from the raw data files.
* To generate plots in main and supplementary figures check the code in /src/Figures

<div align="justify">

## Raw data files

You can find the raw data files on ArrayExpress:
* F1 Chip-Seq [MTAB-14209](https://www.ebi.ac.uk/biostudies/arrayexpress/studies/E-MTAB-14209)
* F1 genomic DNA [MTAB-14210](https://www.ebi.ac.uk/biostudies/arrayexpress/studies/E-MTAB-14210)


<div align="justify">

## Basenji training code

All code used to train the Basenji model is available on this [GitHub repository](https://github.com/ZauggGroup/basenji_drosophila)


<div align="justify">

## Citation

**Integrating genetic variation with deep learning provides context for variants impacting transcription factor binding during embryogenesis** by Olga Sigalova*, Mattia Forneris*, Frosina Stojanovska, Bingqing Zhao, Rebecca R. Viales, Adam Rabinowitz, Fayrouz Hamal, Benoît Ballester, [Judith Zaugg](https://www.zaugg.embl.de/), and [Eileen Furlong](http://furlonglab.embl.de/), *Genome Research*, 2025, [doi: 10.1101/gr.279652.124](https://doi.org/10.1101/gr.279652.124)


